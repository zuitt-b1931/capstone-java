package com.zuitt.batch193;

public class Main {
    public static void main(String[] args){
        Contact contact01 = new Contact("John Doe", "+639123456789", "my home in Quezon City");

        contact01.setNumbers("+639129876543");
        contact01.setAddresses("my office in Makati City");

        Contact contact02 = new Contact("Jane Doe", "+639123456789", "my home in Caloocan City");

        contact02.setNumbers("+639129876543");
        contact02.setAddresses("my office in Pasay City");

        Phonebook phonebook = new Phonebook(){};

        phonebook.setContacts(contact01);
        phonebook.setContacts(contact02);

        phonebook.printPhonebook();
//        contact01.printDetails();
//        contact02.printDetails();
    }
}
