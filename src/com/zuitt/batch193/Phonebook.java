package com.zuitt.batch193;

import java.util.ArrayList;

public class Phonebook extends Contact {
    // Properties
    private ArrayList<Object> contacts = new ArrayList<>();

    // Constructors
    // Default
    public Phonebook(){
        super();
    }

    // Parameterized
    public Phonebook(String name, String numbers, String addresses){
        super(name, numbers, addresses);
    }

    // Getters
    public ArrayList<Object> getContacts() {
        return this.contacts;
    }

    // Setters
    public void setContacts(Object name) { contacts.add(name); }

    // Methods
    public void printPhonebook() {
        if (contacts.isEmpty()){
            System.out.println("This phonebook has no contacts yet.");
        } else {
            for(int i = 0; i < contacts.toArray().length; i++){
                contacts[i].super.printDetails();
            }
        }
    }
}
