package com.zuitt.batch193;

import java.util.ArrayList;

public class Contact {
    // Properties
    private String name;
    private ArrayList<String> numbers = new ArrayList<>();
    private ArrayList<String> addresses = new ArrayList<>();

    // Constructors
    // Default
    public Contact(){};

    // Parameterized
    public Contact(String name, String numbers, String addresses){
        this.name = name;
        this.numbers.add(numbers);
        this.addresses.add(addresses);
    }

    // Getters
    public String getName() { return name; }
    public ArrayList<String> getNumbers = new ArrayList<>();
    public ArrayList<String> getAddresses = new ArrayList<>();

    // Setters
    public void setName(String name) { this.name = name; }
    public void setNumbers(String numbers) { this.numbers.add(numbers); }
    public void setAddresses(String addresses) { this.addresses.add(addresses); }

    // Methods
    public void printNumbers() {
        if (numbers.isEmpty()){
            System.out.println("This contact has no registered numbers.");
        } else {
            for(int i = 0; i < numbers.toArray().length; i++){
                System.out.println(numbers.toArray()[i]);
            }
        }
    }
    public void printAddresses() {
        if (addresses.isEmpty()){
            System.out.println("This contact has no registered addresses.");
        } else {
            for(int i = 0; i < addresses.toArray().length; i++){
                System.out.println(addresses.toArray()[i]);
            }
        }
    }
    public void printDetails() {
        System.out.println(getName());
        System.out.println("--------------------");
        System.out.println(getName() + " has the following registered numbers:");
        printNumbers();
        System.out.println("--------------------");
        System.out.println(getName() + " has the following registered addresses:");
        printAddresses();
        System.out.println("====================");
    }
}
